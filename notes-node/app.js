const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');
const titleOptions = {
    describe: 'The title of your note.',
    demand: true,
    alias: 't'
};
const bodyOptions = {
    describe: 'The body of your note.',
    demand: true,
    alias: 'b'
};

const argv = yargs
.command("add", "Used to Add a note.", {
    title: titleOptions,
    body: bodyOptions
})
.command("list", "Lists all notes.")
.command("read", "Gets a note.", {
    title: titleOptions
})
.command("delete", "Deletes a note.", {
    title: titleOptions
})
.help()
.argv;

var command = process.argv[2];

console.log("Yargs: " + JSON.stringify(argv));

if(command === 'add'){
    var note = notes.addNote(argv.title, argv.body);
    if(note){
        console.log("Note Saved");
        notes.logNote(note);
    } else {
        console.log("A note already exists with that title.");
    }
} else if (command === 'list'){
    var allNotes = notes.getAll();
    console.log(`Printing ${allNotes.length} note(s).`);
    allNotes.forEach((note) => notes.logNote(note));
} else if (command === 'read'){
    var note = notes.getNote(argv.title);
    if (note) {
        console.log("Note Returned");
        notes.logNote(note);
    } else {
        console.log("That note does not exist.");
    }
} else if (command === 'delete'){
    var note = notes.removeNote(argv.title);
    var message = note ? "Note Removed" : "The note did not get removed";
    console.log(message);
} else {
    console.log("Not recognised");
}

