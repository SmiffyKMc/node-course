const fs = require('fs');

var originalNote = {
    title: 'some title',
    body: 'some body'
};

var originalNoteString = JSON.stringify(originalNote);

console.log(typeof originalNoteString);
console.log(originalNoteString);

fs.writeFileSync("playground/notes.json", originalNoteString);

var noteString = fs.readFileSync("playground/notes.json");

var note = JSON.parse(noteString);

console.log(typeof note);
console.log(note.title);