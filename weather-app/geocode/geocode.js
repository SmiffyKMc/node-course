const request = require('request');

var geocodeAddress = (argAddress, callback) => {
    var address = encodeURIComponent(argAddress);
    
    request({
        url: `http://maps.googleapis.com/maps/api/geocode/json?address=${address}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('There was an issue connecting to Googles API.');    
        } else if (body.status === 'ZERO_RESULTS') {
            callback('Unable to find that address.');
        } else if (body.status === 'OK') {
            callback(undefined, {
                address: body.results[0].formatted_address,
                lat: body.results[0].geometry.location.lat,
                lng: body.results[0].geometry.location.lng
            });
        }    
    });
}

module.exports = {
    geocodeAddress,
}