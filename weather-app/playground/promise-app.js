const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

var address = encodeURIComponent(argv.address);
var url = `http://maps.googleapis.com/maps/api/geocode/json?address=${address}`

axios.get(url).then((response) => {
    console.log(response.data);
}).catch((e) => {
    console.log(e);
})