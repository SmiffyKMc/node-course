const request = require('request');

var geocodeAddress = (argAddress) => {
    return new Promise((resolve, reject) => {
        var address = encodeURIComponent(argAddress);
        
        request({
            url: `http://maps.googleapis.com/maps/api/geocode/json?address=${address}`,
            json: true
        }, (error, response, body) => {
            if (error) {
                reject('There was an issue connecting to Googles API.');    
            } else if (body.status === 'ZERO_RESULTS') {
                reject('Unable to find that address.');
            } else if (body.status === 'OK') {
                resolve({
                    address: body.results[0].formatted_address,
                    lat: body.results[0].geometry.location.lat,
                    lng: body.results[0].geometry.location.lng
                });
            }    
        });
    })
}

geocodeAddress('cork city').then((res) => {
    console.log(res);
}).catch((err) => {
    console.log(err);
})