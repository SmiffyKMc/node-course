const request = require('request');

var getWeather = (lat, lng, callback) => {
    request({
        url: `https://api.darksky.net/forecast/7a51a0cad6f39b1abb01bbdc29c596a8/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if(!error && response.statusCode === 200){
            callback(`Temperature: ${body.currently.temperature} Apparent Temperature ${body.currently.apparentTemperature}`);
        } else {
            callback("Unable to get weather");
        }
    })
}

module.exports.getWeather = getWeather;
