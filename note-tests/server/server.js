const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send("Hello World");
});

app.get('/users', (req, res) => {
    res.send([
        {
            name: 'Kieran',
            age: 25
        },
        {
            name: 'Eadaoin',
            age: 26
        },
        {
            name: 'Joe',
            age: 23
        }
    ])
});

app.listen(3000, () => {
    console.log("Listening on Port 3000");
});

module.exports.app = app;