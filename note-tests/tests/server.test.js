const request = require('supertest');
const expect = require('expect');

var app = require('../server/server').app;

describe('Server', () => {

    describe('#route', () => {
        it('should return hello world response', (done) => {
            request(app)
            .get('/')
            .expect(200)
            .expect('Hello World')
            .end(done);
        });
    });
    
    describe('#users', () => {
        it('should return a list of users and say I\'m in it', (done) => {
            request(app)
            .get('/users')
            .expect((res) => {
                expect(res.body).toInclude({
                    name: 'Kieran',
                    age: 25
                });
            })
            .end(done);
        });
    });
    
})

