const expect = require('expect');
const rewire = require('rewire');

var app = rewire('../spies/app');

describe('App', () => {

    it('should call the spy correctly', () => {
        var spy = expect.createSpy();
        spy('Kieran', 25);
        expect(spy).toHaveBeenCalledWith('Kieran', 25);
    })
})