const utils = require('../utils/utils');
const expect = require('expect');

describe('Utils', () => {
    it('should add two numbers', () => {
        var res = utils.add(1, 2);
    
        expect(res).toBe(3).toBeA('number');
    });
    
    it('should add two numbers eventually', (done) => {
        utils.asyncAdd(1, 1, (res) => {
            expect(res).toBe(2).toBeA('number');
            done();
        });
    });
    
    it('should multiply two numbers', () => {
        var res = utils.mult(2, 55);
    
        expect(res).toBe(110).toBeA('number');
    });
    
    it('should square a number', () => {
        var res = utils.square(4);
    
        expect(res).toBe(16).toBeA('number');
    });
    
    it('should square a number eventually', (done) => {
        utils.asyncSquare(4, (res) => {
            expect(res).toBe(16).toBeA('number');
            done();
        });
    })
})



