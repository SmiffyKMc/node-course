const express = require("express");
const hbs = require("hbs");
const fs = require("fs");

const app = express();

hbs.registerPartials(__dirname + '/views/partials')

app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});
hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase();
});

app.use((req, res, next) => {
    var now = new Date();
    var log = `${now}: ${req.url}: ${req.method}`;

    console.log(log);

    fs.appendFile('server.log', log + '\n', (err) => {
        if(err){
            console.log(err);
        }
    });

    next();
});

app.get('/', (req, res) => {
    res.render('home', {
        welcome: "Welcome to my page!"
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        welcome: 'Just some text from the server'
    });
});

app.get('/projects', (req, res) => {
    res.render('projects', {
        welcome: "Welcome to the Projects page."
    })
});

app.listen(3000, () => {
    console.log(`Server listing on port 3000`);
});